# Flutter Mobile School

https://mschool.codemagic.app

## Preview

[Download the release APK to try out on your android phone](https://install.appcenter.ms/users/momenamiin/apps/flutter_slides_portrait/distribution_groups/public) 

or by scaning the following qr code 

![frame (5)](https://user-images.githubusercontent.com/18642838/148647786-a790d836-1f7f-461c-8d50-1e00fc893e9b.png)
